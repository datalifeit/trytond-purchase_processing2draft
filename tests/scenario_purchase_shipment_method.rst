==================================
Purchase Processing2Draft Scenario
==================================

Imports::

    >>> import datetime
    >>> import os
    >>> from trytond.tests.tools import activate_modules
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from operator import attrgetter
    >>> from proteus import Model, Wizard, Report
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> from trytond.exceptions import UserError
    >>> today = datetime.date.today()


Install purchase_processing2draft::

    >>> config = activate_modules(['purchase_processing2draft', 'purchase_shipment_method'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')


Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']
    >>> cash = accounts['cash']

    >>> Journal = Model.get('account.journal')
    >>> PaymentMethod = Model.get('account.invoice.payment.method')
    >>> cash_journal, = Journal.find([('type', '=', 'cash')])
    >>> cash_journal.save()
    >>> payment_method = PaymentMethod()
    >>> payment_method.name = 'Cash'
    >>> payment_method.journal = cash_journal
    >>> payment_method.credit_account = cash
    >>> payment_method.debit_account = cash
    >>> payment_method.save()


Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()


Create parties::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier')
    >>> supplier.save()
    >>> customer = Party(name='Customer')
    >>> customer.customer_tax_rule = None
    >>> customer.save()


Create account categories::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

    >>> account_category_tax, = account_category.duplicate()
    >>> account_category_tax.customer_taxes.append(tax)
    >>> account_category_tax.save()


Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> gram, = ProductUom.find([('name', '=', 'Gram')])
    >>> kilo, = ProductUom.find([('name', '=', 'Kilogram')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> template = ProductTemplate()
    >>> template.name = 'PROD1'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.purchasable = True
    >>> template.list_price = Decimal('10.0')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> product1, = template.products
    >>> product1.code = 'PROD1'
    >>> product1.cost_price = Decimal('5.0')
    >>> product1.save()
    >>> product2 = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'PROD2'
    >>> template.default_uom = gram
    >>> template.type = 'goods'
    >>> template.purchasable = True
    >>> template.list_price = Decimal('10.0')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> product2, = template.products
    >>> product2.code = 'PROD2'
    >>> product2.cost_price = Decimal('5.0')
    >>> product2.save()


Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()


Create a purchase and process::

    >>> Purchase = Model.get('purchase.purchase')
    >>> PurchaseLine = Model.get('purchase.line')
    >>> purchase = Purchase()
    >>> purchase.party = customer
    >>> purchase.payment_term = payment_term
    >>> purchase_line = PurchaseLine()
    >>> purchase.lines.append(purchase_line)
    >>> purchase_line.product = product1
    >>> purchase_line.quantity = 2.0
    >>> purchase_line.unit_price = Decimal(1)
    >>> purchase_line = PurchaseLine()
    >>> purchase.lines.append(purchase_line)
    >>> purchase_line.product = product2
    >>> purchase_line.quantity = 20.0
    >>> purchase_line.unit_price = Decimal(10)
    >>> purchase.click('quote')
    >>> purchase.click('confirm')
    >>> purchase.click('process')


Check shipment::

    >>> shipment, = purchase.shipments
    >>> shipment.click('receive')
    >>> number = shipment.number
    >>> not purchase.shipment_number_cache
    True


Draft processing purchase::

    >>> purchase.click('draft')
    >>> purchase.state
    'draft'
    >>> len(purchase.shipments), len(purchase.shipment_returns), len(purchase.invoices)
    (0, 0, 0)
    >>> purchase.shipment_number_cache == number
    True


Process again and check shipment number::

    >>> purchase.click('quote')
    >>> purchase.click('confirm')
    >>> purchase.click('process')
    >>> purchase.shipments[0].number == number
    True