# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import purchase


def register():
    Pool.register(
        purchase.Purchase,
        module='purchase_processing2draft', type_='model')
    Pool.register(
        purchase.Purchase2,
        module='purchase_processing2draft', type_='model',
        depends=['purchase_shipment_method'])
